#include<iostream>
#include<string>
#include<vector>
#include<time.h>
#include<conio.h>
#include"UnorderedArray.h"

using namespace std;

void main()
{
	int userInputArraySize;

	cout << "How big is the size you want your array to have?" << endl;
	cout << "Array Size:";
	cin >> userInputArraySize;

	UnorderedArray<int> number(userInputArraySize);

	for (int i = 1; i <= userInputArraySize; i++)
	{
		int randomValues = rand() % 100 + 1;
		number.push(randomValues);
	}

	for (int i = 0; i < number.getSize(); i++)
	{
		cout << number[i] << endl;
	}

	int userInputFind;
	cout << "You must remove one of your element inside the array!" << endl;
	cout << "Array Index:";
	cin >> userInputFind;
	number.remove(userInputFind - 1);

	cout << "========MODIFIED============" << endl;

	for (int i = 0; i < number.getSize(); i++)
	{
		cout << number[i] << endl;
	}

	int searchElement;
	cout << "Give a number to search in the array:";
	cin >> searchElement;

	number.linearSearch(searchElement);

	_getch();
}