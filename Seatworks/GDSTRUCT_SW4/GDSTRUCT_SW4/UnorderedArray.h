#pragma once
#include<iostream>
#include<assert.h>
//it assert things

using namespace std;

template<class T> // it allows unordered array to accept any class to be its data
class UnorderedArray
{
public:
	UnorderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0) // This means we initialize all of them in the constructor
	{
		if (size)
		{
			mMaxSize = size;

			mArray = new T[mMaxSize]; //Allocates memory
			memset(mArray, 0, sizeof(T) * mMaxSize); //Memset allocates memory and sets a value

			mGrowSize = ((growBy > 0) ? growBy : 0); //Question mark = ternary operator, single line IF OR ELSE STATEMENT
		}
	}

	virtual ~UnorderedArray() //Destructor Deletes the ones in the class
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
		{
			expand();
		}

		mArray[mNumElements] = value;
		mNumElements++;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	int getSize()
	{
		return mNumElements;
	}

	virtual void pop()
	{
		if (mNumElements > 0)
		{
			mNumElements--;
		}
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL);

		if (index >= mMaxSize) // index is more than the current size
		{
			return;
		}

		for (int i = index; i < mMaxSize - 1; i++)
		{
			mArray[i] = mArray[i + 1];
		}

		mMaxSize--;

		if (mNumElements >= mMaxSize)
		{
			mNumElements = mMaxSize;
		}
	}

	int linearSearch(int input)
	{
		for (int i = 0; i <= input; i++)
		{
			if (mArray[i] == input)
			{
				cout << "Search found at index " << i + 1;
				return i;
			}
			else
			{
				cout << "404 not found.";
				break;
			}
		}
	}

private: // "m" stands member of private class
	T* mArray; // What is the data type of T? Anything we assign
	int mMaxSize;
	int mGrowSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
		{
			return false;
		}

		T* temp = new T[mMaxSize + mGrowSize];

		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;

		return true;
	}
};