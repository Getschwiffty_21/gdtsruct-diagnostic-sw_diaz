#include<iostream>
#include<time.h>
#include<conio.h>
using namespace std;



int main()
{
	srand(time(NULL));

	int array[50];
	int n = 10;
	int i;
	int j;
	int temp;
	char choice;
	int search;

	for (i = 0; i < n; ++i)
	{
		array[i] = rand() % 69 + 1;
		cout << " " << array[i];
	}
	_getch();
	cout << "\n Do you want to sort this in an ascending or descending manner?" << endl;
	cout << "[A] for Ascending" << "\n[D] for Descending" << "\nINPUT:";
	cin >> choice;
	
	if (choice == 'a' || choice == 'A')
	{
		for (i = 1; i < n; ++i)
		{
			for (j = 0; j < (n - i); ++j)
				if (array[j] > array[j + 1])
				{
					temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
				}
		}

		for (i = 0; i < n; ++i)
			cout << " " << array[i];
	}

	if (choice == 'd' || choice == 'D')
	{
		for (i = 1; i < n; ++i)
		{
			for (j = 0; j < (n - i); ++j)
				if (array[j] < array[j + 1])
				{
					temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
				}
		}

		for (i = 0; i < n; ++i)
			cout << " " << array[i];
	}
	_getch();

	cout << "\nEnter a number to search in the array:";
	cin >> search;

	int counter = 1;
	for (i = 0; i < n ; i++)
	{
		if (search == array[i])
		{
			cout << "Search found" << "\nSteps: " << counter;
		}

		else if (search != array[i])
		{
			cout << "Search not found";
			break;
		}
	}

	_getch();
	return 0;
}